class ProductService {
   static products = [
    {
      sno: "Ad1",
      name: "Apple Watch",
      imageUrl:
        "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/apple-watch-series7-modular-face-09142021-1634324247.jpg?crop=1xw:1xh;center,top&resize=640:*",
      price: 1500,
      qty: 3,
    },
    {
      sno: "Ad2",
      name: "Realme Watch",
      imageUrl:
        "https://ae01.alicdn.com/kf/Hce11ab5a82a44261800e3f4634856389U/Original-OPPO-Watch-41mm-46mm-eSIM-Cell-phone-1-6inch-AMOLED-Snapdragon-2500-Apollo-3-VOOC.jpg_Q90.jpg_.webp",
      price: 1800,
      qty: 2,
    },
    {
      sno: "Ad3",
      name: "Samsung Watch",
      imageUrl:
        "https://i.gadgets360cdn.com/large/oppo_watch_title_1583501087321.jpg",
      price: 2500,
      qty: 5,
    },
    {
      sno: "Ad4",
      name: "Lg Watch",
      imageUrl: "https://cmobileprice.com/products/OPPO_Watch_1.jpg",
      price: 2340,
      qty: 4,
    },
    {
      sno: "Ad5",
      name: "Oppo Watch",
      imageUrl:
        "https://assorted.downloads.oppo.com/static/assets/products/oppo-watch-1/images/1920/sec8-img-073222c6333cda52cedac1e01048df38.png",
      price: 1900,
      qty: 7,
    },
  ];
  static getAllProducts(){
    return this.products;
  }
}

export default ProductService