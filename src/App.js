import './App.css';
import { CourseSelection } from './components/CourseSelection';
import { ProductListTable } from './components/ProductListTable';
import ShoppingCard from './components/ShoppingCard';

function App() {
  return (
    <div>
      <nav className="navbar navbar-dark bg-dark navbar-expand-sm">
        <a href="/" className="navbar-brand">
          React with Events Handling
        </a>
      </nav>
      {/* <CourseSelection  /> */}
      {/* <ProductListTable  /> */}
      <ShoppingCard />
    </div>
  );
}

export default App;
