import React,{useState} from 'react'
import { FaMinusCircle, FaPlusCircle, FaTrash } from 'react-icons/fa';
import ProductService from "../services/ProductService";
const ShoppingCard =()=> {
    let [state, setState] = useState({
      products: ProductService.getAllProducts()
    });

    let incrProductQty = (sno) => {
        let incrProducts = state.products.map(product => {
            if(product.sno===sno){
                return {
                    ...product,
                    qty:product.qty+1
                }
            }
            return product
        })
        setState({
            products:incrProducts
        })
    }
    let decrProductQty = (sno) => {
        let incrProducts = state.products.map(product => {
            if(product.sno===sno){
                return {
                    ...product,
                    qty:product.qty > 0 ? product.qty-1:1
                }
            }
            return product
        })
        setState({
            products:incrProducts
        })
    }
    let deleteProduct = (sno) => {
        let remainingProducts = state.products.filter(product =>{
            return product.sno !== sno
        });
        setState({
            products: remainingProducts
        })
    }

    let grandTotal= () => {
        let total=0;
        for(let product of state.products){
            total += product.price * product.qty
        }
        return total
    }
    return (
      <div>
        <section className="mt-3">
          <div className="container">
            <div className="row">
              <div className="col">
                <p className="h4 text-success">Shopping Card</p>
                <p>
                    {/*{JSON.stringify(state.products)}*/}
                  This is event handling part where user can increase or
                  decrease the quantity as they needed, which is automatically add to total and there is grand
                    total where shown the all total price. We call the increment and decrement function in different
                    way like binding and by arrow function. we use the fileter mathod to delete the data.
                </p>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="container">
            <div className="row">
              <div className="col">
                <table className="table table-hover text-center table-striped">
                  <thead className="bg-success text-white">
                    <tr>
                      <th>SN</th>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Price</th>
                      <th>Qty</th>
                      <th>Total</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                    <tbody>
                    {
                        state.products.length > 0 && state.products.map(product =>{
                            return (
                                <tr key={product.sno}>
                                    <td>{product.sno}</td>
                                    <td>
                                        <img
                                            src={product.imageUrl}
                                            width={90}
                                            height={90}
                                        />
                                    </td>
                                    <td>{product.name}</td>
                                    <td>{product.price}</td>
                                    <td>
                                        <FaMinusCircle onClick={decrProductQty.bind(this, product.sno)} style={{ color: "#28a745" }}  />
                                        {product.qty}
                                        <FaPlusCircle onClick={()=>incrProductQty(product.sno)} style={{ color: "#28a745" }} />
                                    </td>

                                    <td>{product.qty * product.price}</td>
                                    <td>
                                        <FaTrash onClick={()=>deleteProduct(product.sno)} style={{color:"red"}} />
                                    </td>
                                </tr>
                            );
                        })
                    }
                    <tr>
                        <td colSpan={4}></td>
                        <td>Grand Total</td>
                        <td>{grandTotal()}</td>
                    </tr>
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
}

export default  ShoppingCard