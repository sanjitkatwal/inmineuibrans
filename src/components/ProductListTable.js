import React, {useState} from 'react'
import {FaPlusCircle, FaMinusCircle} from 'react-icons/fa'

export const ProductListTable = () => {
let [state, setState] = useState({
  product: {
    sno: "Ad1",
    name: "Apple Watch",
    imageUrl:"https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/apple-watch-series7-modular-face-09142021-1634324247.jpg?crop=1xw:1xh;center,top&resize=640:*",
    price: 1500,
    qty: 3,
  },
});

let incrQty = () =>{
    setState({
      product: {
        ...product,
        qty: state.product.qty+1,
      },
    });
}
let decrQty = () => {
  setState({
    product: {
      ...product,
      qty: state.product.qty > 0 ? state.product.qty-1:1,
    },
  });
};

 let { product } = state;
    return (
      <div>
        <h2>Product List Item</h2>
        <pre>{JSON.stringify(product)}</pre>
        <section className="mt-3">
          <div className="container">
            <div className="row">
              <div className="col">
                <p className="h4 text-success">Product Item</p>
                <p>
                  This is event handling part where user can increase or decrease the quantity
                  as they needed.
                </p>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="row">
              <div className="col">
                <table className="table table-hover text-center table-striped">
                  <thead className="bg-success text-white">
                    <tr>
                      <th>SN</th>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Price</th>
                      <th>Qty</th>
                      <th>Total</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{product.sno}</td>
                      <td>
                        <img src={product.imageUrl} width={90} height={90} />
                      </td>
                      <td>{product.name}</td>
                      <td>{product.price}</td>
                      <td>
                        <FaMinusCircle
                          onClick={decrQty}
                          style={{ color: "#28a745" }}
                        />
                        {product.qty}
                        <FaPlusCircle
                          onClick={incrQty}
                          style={{ color: "#28a745" }}
                        />
                      </td>
                      <td>{product.qty * product.price}</td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
}
