import React, {useState} from 'react'

export const CourseSelection = () => {
     const [state, setState] = useState({
         courseName:'Nothing'
     })

     let vueCourse = () =>{
         setState({
             courseName:'Vue js'
         })
     }

     let courseSelection = (name)=>{
        setState({
            courseName:name
        })
     }
    return (
      <div>
        <React.Fragment>
          <section>
            <div className="container">
              <div className="row">
                <div className="col-md-6 mt-3">
                  <div className="card">
                    <div className="card-header bg-info">
                      <p className="h4">Course Selector</p>
                    </div>
                    <div className="card-body bg-light">
                        <p>In this section we worked with function with parameter, function withoud parameter, props binding, and function calling in different button and actions.</p>
                      <h5 className="display-5">
                        I Learn : {state.courseName}
                      </h5>
                      <button
                        onClick={vueCourse}
                        className="btn btn-success btn-sm"
                      >
                        Vue JS
                      </button>
                      <button
                        onClick={courseSelection.bind(this, "React JS")}
                        className="btn btn-primary btn-sm"
                      >
                        React JS
                      </button>
                      <button
                        onClick={() => {
                          setState({ courseName: "Angular" });
                        }}
                        className="btn btn-danger btn-sm"
                      >
                        Angular
                      </button>
                      <button
                        onClick={() => {
                          courseSelection('Java');
                        }}
                        className="btn btn-danger btn-sm"
                      >
                        Java
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </React.Fragment>
      </div>
    );
}
